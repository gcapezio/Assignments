class btree:
    def __init__(self,k,leaf):
        self.__degree = k #"k-ary tree" with a degree value of k (same for whole tree)
        self.__currentchild = 0 #the number of children the current node has
        self.__keys = [] #array of keys within the current node
        self.__leaf = leaf #boolean value on whether or not it's a leaf
        self.__child = [] #pointers to the child nodes
 
    def insert(self,key):
        if(self.__currentchild == (2*self.__degree - 1) ):      #number of keys in current nodes are full, so we have to split
            s = btree(self.__degree,False) #allocate new node that is not a leaf
            s.__child.append(self) #append the current node as the new nodes child
 
            s.splitChild(0,self)        #spliiting the child now (the full one)
            i = 0
                                    #s node will contain 2 pointers to the 2 children
            if(s.__keys[0] < key): #check which child to insert the new key(larger=2nd key, smaller=1st key)
                i += 1
            s.__child[i] = s.__child[i].insertNonFull(key)      #insert the value accordingly
 
            self = s #our new root is s
 
        else:            
            self = self.insertNonFull(key)      # if not full then insert in current node
        return self
 
    def insertNonFull(self,k):  #function to insert in current node if not full
        i = self.__currentchild - 1 #store current child with index starting at 0
 
        if(self.__leaf == True):        #if leaf then add it 
            if(( i>=0 and self.__keys[i] > k)): #if the last key is smaller than the value of k
                self.__keys.append(None) #increases the array length by one to accomodate for the last element
            while( i>=0 and self.__keys[i] > k): #finds the correct location of the key k to be insert
                self.__keys[i+1] = self.__keys[i] #shift to the right to make room for new key
                i -= 1 #going right to left
            if(self.__currentchild <= i+1): #if k is the largest key
                self.__keys.append(k) #add it to the end
            else:
                self.__keys[i+1] = k # store new key k 
            self.__currentchild += 1 #increments the child count by one
 
        else:       #else we will have to split 
            while (i >= 0 and self.__keys[i] > k): #finds correct location of key to be inserted
                i -= 1
            if(self.__child[i+1].__currentchild == self.__degree*2 - 1) : #if the child node in which the key will be inserted is full or not
                self.splitChild(i+1,self.__child[i+1]) #split it
                if (self.__keys[i+1] < k): #insert key before of after the sole key
                    i += 1
            self.__child[i+1] = self.__child[i+1].insertNonFull(k); #insert the key k into this position
 
        return self
 
    def splitChild(self,i,root):        # splitting the root with parent node: self and child to be splitt: child
        z = btree(self.__degree,root.__leaf) # new child to be created and added to self
        z.__currentchild = self.__degree - 1 #same degree as parent but currentchild will be one less then degree
        j = 0
        while j < self.__degree-1:          #appending keys to newly formed node z (right child)
            z.__keys.append(root.__keys[j+self.__degree]) #add the keys in
            j += 1
 
        if(root.__leaf == False):   #if the left child root is not a leaf
            j = 0
            while j < self.__degree:
                z.__keys.append(root.__keys[j+self.__degree])# adding righmost degree - 1 keys to z
                j += 1
 
        root.__currentchild = self.__degree - 1 #root will also contains degree-1 keys, so changing number of children
        temp = root.__keys[self.__degree - 1] # storing the median key
        while(len(root.__keys) > root.__currentchild):      #deleting extra keys from root which are stored in node z
            root.__keys.pop()       #pop function deletes the elemetn of array from right, but righmost keys are already present in the z node, so that's ok
        j = self.__currentchild
 
        #input i is the current number of keys in self node
        if(j >= i+1): #needed to create extra space in child array, so that when shifting of element is needed, we can shift them to right, otherwise it will not be possible
            self.__child.append(None)   #added extra space in child array in the end i.e. the rightmost position
 
        while j >= i+1:                 #changing child positions accordingly
            self.__child[j+1] = self.__child[j] #shifts the current keys to the right
            j -= 1
 
        self.__child.append(z)      #appending newly formed child to current node
 
        j = self.__currentchild - 1
        if(j >= i): #needed to create extra space in key array
            self.__keys.append(None) #add the extra space to the end (rightmost)
 
 
        while j>=i:             #and appending keys to current node and finally
            self.__keys[j+1] = self.__keys[j] #shift the keys down by one to the right
            j -= 1
 
        self.__keys.append(temp) #add in the median key
        self.__currentchild += 1  #incrementing child count
 
 
    def search(self, key):
        i = 0
        while (i < self.__currentchild and key > self.__keys[i]):#find the position (correct index) in the keys that the new key fits
            i += 1
 
        if(len(self.__keys) > i): #if the key is between the key values
            if (self.__keys[i] == key): #if that index contains the key
                print("\nPresent")
                return 1
            if (self.__leaf == True): #if it's a leaf, can't go any further 
                print("\nNot Present")
                return 1
        if(len(self.__child) >= i):#if the value of the key you are searching for is larger than all the key values in the node
            return self.__child[i].search(key) #search the child node for the value
        return 0
    def printtree(self):
        i = 0
        while(i < self.__currentchild):
            if(self.__leaf == False):
                self.__child[i].printtree()
            print(" "+str(self.__keys[i]))
            i += 1
        if(self.__leaf == False):
            self.__child[i].printtree()
 
 
def main():
    root = btree(3,True)
 
    root = root.insert(1)
    root = root.insert(3)
    root = root.insert(5)
    root = root.insert(7)
    root = root.insert(9)
    root = root.insert(8)
    root = root.insert(6)
    root = root.insert(4)
    root = root.insert(2)
    root = root.insert(10)
    root = root.insert(12)
    root = root.insert(11)
    root = root.insert(13)
    root.printtree()
    if(root.search(18) == 0):
        print("\nNot Present")
    if(root.search(1) == 0):
        print("\nNot Present")
    if(root.search(8) == 0):
        print("\nNot Present")

