#creating lists of increasing size
list_1 = [3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 123, 127, 130, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57,3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120]
list_2 = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128,  256, 512, 1024, 2048, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114,  256, 512, 1024, 2048, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120, 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36, 39, 42,  45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75, 78, 81, 84, 87, 90, 93, 96, 99, 102, 105, 108, 111, 114, 117, 120]
#begin adding lists together to add to size
list_3 = list_1 + list_2
list_4 = list_1 + list_2 + [20]
list_5 = list_1 + list_2 + list_3 + [9, 14]
list_6 = list_1 + list_2 + list_3 + list_4 + [7,8,9] 
list_7 = list_1 + list_2 + list_3 + list_4 + list_5 + [11,22,33,44]
list_8 = list_1 + list_2 + list_3 + list_4 + list_5 + list_6 + [60, 120, 180, 360, 720]
list_9 = list_1 + list_2 + list_3 + list_4 + list_5 + list_6 + list_7 + [3] * 6
list_10 = list_1 + list_2 + list_3 + list_4 + list_5 + list_6 + list_7 + list_8 + [22, 32] * 4


#define functions for my_min and my_min_slow

def my_min(list):
    min_so_far = list[0] 
    for i in range(1,len(list)):
        if list[i] < min_so_far: 
             min_so_far = list[i]
        return min_so_far

def my_min_slow(list):
    min_so_far = list[0]  
    for j in range(1,len(list)+1):
        for i in range(0,j):
            if list[i] < min_so_far:
                min_so_far = list[i]
    return min_so_far

#run functions on lists

import time 

print len(list_1)
start = time.time()
(my_min(list_1))
stop = time.time()
print (stop - start)
#finding runtime for my_min
start = time.time()
my_min_slow(list_1)
stop = time.time()
print (stop - start)
#finding runtime for my_min_slow

print len(list_2)
start = time.time()
my_min(list_2)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_2)
stop = time.time()
print (stop - start)

print len(list_3)
start = time.time()
my_min(list_3)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_3)
stop = time.time()
print (stop - start)

print len(list_4)
start = time.time()
my_min(list_4)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_4)
stop = time.time()
print (stop - start)

print len(list_5)
start = time.time()
my_min(list_5)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_5)
stop = time.time()
print (stop - start)

print len(list_6)
start = time.time()
my_min(list_6)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_6)
stop = time.time()
print (stop - start)

print len(list_7)
start = time.time()
my_min(list_7)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_7)
stop = time.time()
print (stop - start)

print len(list_8)
start = time.time()
my_min(list_8)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_8)
stop = time.time()
print (stop - start)

print len(list_9)
start = time.time()
my_min(list_9)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_9)
stop = time.time()
print (stop - start)

print len(list_10)
start = time.time()
my_min(list_10)
stop = time.time()
print (stop - start)
start = time.time()
my_min_slow(list_10)
stop = time.time()
print (stop - start)