class ListNode:#define class 
    def __init__(self, data): #set initial condtion 
        self.data = data
        self.next = None

    def print_linked_list_pretty(self):#function for later testing
        current= self
        while(current!=None):
            print current.data
            current = current.next
        print "None"

    
        
class Queue: #new class Queue
    def __init__(self):#inital condition
        self.front = None
        self.rear = None
        self.size = 0
        
    def enqueue(self, item):       
        n = ListNode(item)  #make number into a node 
        if self.size == 0: #base case
            self.front = n #one node 
        else:
            n = self.rear.next #number will be next node
            n = self.rear #number will become rear, end of list, no return statement
            self.size += 1 #add one to size for every added enqueue item
            
    def dequeue(self):
        if self.size == 0 : #base case, no size, empty queue
            return None
        else:
            temp = self.front #temporary, front of queue 
            self.front = self.front.next #front item removed, next node becomes front
            return temp #returns original front item 
            self.size -= 1 #for every removed node subtract one from size
    
    def isEmpty():
        if self.size == 0 :#if size of queue is empty, true
            return True
        else: 
            return False #if queue is not empty, false


        

       
queue = Queue()
queue.enqueue('9')
queue.enqueue('1')
queue.enqueue('8')
queue.dequeue()
queue.front.print_linked_list_pretty()

#These three methodshave a constant runntime because there are have no for loops
#and they all work one node at a time .
